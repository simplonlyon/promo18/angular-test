import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RandomizerService } from '../randomizer.service';

import { ClickChangeComponent } from './click-change.component';

describe('ClickChangeComponent', () => {
  let component: ClickChangeComponent;
  let fixture: ComponentFixture<ClickChangeComponent>;
  let compiled:HTMLElement;
  //On crée un spy, c'est à dire, une fausse version du service sur laquelle on a tous les contrôles
  let randomizerSpy = jasmine.createSpyObj<RandomizerService>('RandomizerService', ['getRandom']);
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClickChangeComponent ],
      providers: [
        {provide: RandomizerService, useValue: randomizerSpy} //On assigne notre spy à l'injecteur d'angular
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    //On dit à notre spy que sa méthode getRandom renverra une valeur connue, ici 3
    randomizerSpy.getRandom.and.returnValue(3);
    fixture = TestBed.createComponent(ClickChangeComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Coucou"', () => {
    expect(compiled.querySelector('p')?.textContent).toBe('Coucou'); //test précis
    expect(compiled.innerHTML).toContain('Coucou'); //test vague
  });

  it('should change rendered message on action', () => {
    component.action();
    fixture.detectChanges(); //Le detectChanges, met à jour le template

    expect(component.message).toBe('autre chose');
    expect(compiled.innerHTML).toContain('autre chose'); 

  });

  /**
   * Autre manière de faire le même test qu'au dessus mais en se basant uniquement
   * sur l'interface utilisateur·ice plutôt que sur les méthodes/propriétés du component.
   * Il n'est clairement pas nécessaire de faire les 2, juste on choisit celle qu'on préfère
   * et on fait que celle là (et on peut se dire que celle qu'on préfère c'est la plus simple
   * et rapide à tester)
   */
  it('should change rendered message on button click', () => {
    fixture.debugElement.query(By.css('button')).triggerEventHandler('click', null);
    fixture.detectChanges(); //Le detectChanges, met à jour le template

    expect(compiled.innerHTML).toContain('autre chose'); 

  });

  it('should display random number', () => {
    const rendered = compiled.querySelector('#number')?.textContent;
    expect(rendered).toBe('3');
    expect(randomizerSpy.getRandom).toHaveBeenCalled();
  })
});
