import { Component, OnInit } from '@angular/core';
import { RandomizerService } from '../randomizer.service';

@Component({
  selector: 'app-click-change',
  templateUrl: './click-change.component.html',
  styleUrls: ['./click-change.component.css']
})
export class ClickChangeComponent implements OnInit {

  message = 'Coucou';
  value = 0;

  constructor(private randomizer:RandomizerService) { }

  ngOnInit(): void {
    this.value = this.randomizer.getRandom(1, 10);
  }

  action() {
    this.message = 'autre chose';
  }

}
