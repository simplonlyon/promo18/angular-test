export interface Book {
    title: string;
    first_publish_year: number;
    author_name?: string[];
    cover_i?:number;
    subject?:string[];
}