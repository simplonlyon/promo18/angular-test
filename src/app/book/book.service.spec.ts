import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

import { BookService } from './book.service';

describe('BookService', () => {
  let service: BookService;
  /**
   * Le HttpTestingController va nous permettre de contrôler les requêtes http
   * faites par le HttpClient (et aussi de ne pas les faire pour de vrai en fait)
   * On va pouvoir lui dire que telle url est sensée être appelée et renvoyer telle
   * valeur puis vérifier qu'elle l'a bien été
   */
  let controller:HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(BookService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a search result from api', () => {
    
     // On appel la méthode de notre service
    service.search('test').subscribe((data) => {
      //On vérifie dans le subscribe qu'on a bien ce qu'on pense avoir récupéré
      expect(data.docs).toHaveSize(2);
    });
    //On dit à notre controller http que cette url est sensée avoir été appelée
    const req = controller.expectOne('http://openlibrary.org/search.json?q=test');
    //et qu'elle doit renvoyer ça comme valeur
    req.flush({
      numFounds: 2,
      docs: [{title:'test'}, {title:'test2'}]
    });

  });

  afterEach(() => {
    //On vérifie que les url données ont bien été appelées
    controller.verify();
  })
});
