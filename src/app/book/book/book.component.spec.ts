import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Book } from '../book';

import { BookComponent } from './book.component';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let testBook:Book;
  let compiled:HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    //On remet à zéro la valeur du book de test entre chaque test, sinon ça pourrait poser problème
    testBook  = {
      title: 'title',
      author_name: ['author'],
      cover_i: 1,
      subject: ['subject'],
      first_publish_year: 2022
    };
    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render book informations', () => {
    component.book = testBook;
    fixture.detectChanges();
    expect(compiled.querySelector('h3')?.textContent).toBe('title');
    expect(compiled.innerHTML).toContain('Author: author');
    expect(compiled.innerHTML).toContain('Genres: subject');
    expect(compiled.innerHTML).toContain('Published: 2022');
  });

  it('should display unknown author when no author', () => {
    component.book = testBook;
    component.book.author_name = undefined;
    fixture.detectChanges();
    expect(compiled.innerHTML).toContain('Author: unknown');

  });

  it('should not display genres if no subject', () => {
    component.book = testBook;
    component.book.subject = undefined;
    fixture.detectChanges();
    expect(compiled.innerHTML).not.toContain('Genres:');

  });

  it('should not img if no cover_i', () => {
    component.book = testBook;
    component.book.cover_i = undefined;
    fixture.detectChanges();
    expect(compiled.querySelector('img')).toBeFalsy();

  });
});
