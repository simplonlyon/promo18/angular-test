import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { SearchPageComponent } from './search-page/search-page.component';
import { BookRoutingModule } from './book-routing.module';
import { BookComponent } from './book/book.component';
import { FormsModule } from '@angular/forms';

/**
 * Dans un soucis d'organisation et de portabilité du code, on peut créer des sous
 * modules dans un projet qui contiendront tous les components/services/routes/pipes/etc.
 * liés à une thématique ou une fonctionnalité donnée.
 * Ici on fait un BookModule qui contiendra les différents éléments liés à la recherche
 * de livre, ce module possède son propre router qui déclare ses routes. (l'idée est de
 * pouvoir très facilement prendre ce module pour le mettre dans un autre projet et que
 * tout marche directement, si besoin)
 */
@NgModule({
  declarations: [
    SearchPageComponent,
    BookComponent
  ],
  imports: [
    CommonModule,
    BookRoutingModule,
    HttpClientModule,
    FormsModule
  ]
})
export class BookModule { }
