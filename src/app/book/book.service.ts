import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './book';

export interface SearchResult {
  numFound:number;
  docs: Book[];
}

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http:HttpClient) { }

  search(query:string) {
    return this.http.get<SearchResult>('http://openlibrary.org/search.json?q='+query);
  }
  
}
