import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { BookService } from '../book.service';

import { SearchPageComponent } from './search-page.component';

let testData = {
  numFound: 2,
  docs: [{
    title: 'title',
    author_name: ['author'],
    cover_i: 1,
    subject: ['subject'],
    first_publish_year: 2022
  },
  {
    title: 'title',
    author_name: ['author'],
    cover_i: 1,
    subject: ['subject'],
    first_publish_year: 2022
  }]
};


describe('SearchPageComponent', () => {
  let component: SearchPageComponent;
  let fixture: ComponentFixture<SearchPageComponent>;
  let bookServiceSpy = jasmine.createSpyObj<BookService>('BookService', ['search']);
  let compiled: HTMLElement;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchPageComponent],
      providers: [
        { provide: BookService, useValue: bookServiceSpy }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPageComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display results on search', () => {
    bookServiceSpy.search.and.returnValue(of(testData));

    component.query = 'title';
    component.search();
    fixture.detectChanges();

    expect(bookServiceSpy.search).toHaveBeenCalledWith('title');
    expect(compiled.querySelector('h2')).toBeTruthy();

    expect(compiled.querySelectorAll('app-book')).toHaveSize(2);

  });

  it('should display no result when api send no book', () => {
    bookServiceSpy.search.and.returnValue(of({numFound:0, docs: []}));

    component.query = 'title';
    component.search();
    fixture.detectChanges();

    expect(bookServiceSpy.search).toHaveBeenCalledWith('title');

    expect(compiled.innerHTML).toContain('No results');
  })
});
