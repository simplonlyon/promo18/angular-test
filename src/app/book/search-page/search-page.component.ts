import { Component, OnInit } from '@angular/core';
import { BookService, SearchResult } from '../book.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
  searchResult?:SearchResult;
  query = '';

  constructor(private bookService:BookService) { }

  ngOnInit(): void {
  }

  search() {
    this.bookService.search(this.query).subscribe(data => this.searchResult=data);

  }

}
