import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPageComponent } from './search-page/search-page.component';
/**
 * Dans ce BookRoutingModule, on vient déclarer spécifiquement les routes propres
 * aux books. Sinon il fonctionne exactement comme le AppRoutingModule auquel on est
 * habitué
 */
const routes: Routes = [
  {path: 'search', component: SearchPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)], //Seule différence, le forChild au lieu du forRoot, car c'est un sous routeur
  exports: [RouterModule]
})
export class BookRoutingModule { }
